﻿using Building.API;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Owner.FuntionalTest
{
    public class OwnerScenariosBase
    {
        public TestServer CreateServer()
        {
            var path = Assembly.GetAssembly(typeof(OwnerScenariosBase))
                .Location;

            var hostBuilder = new WebHostBuilder()
                .UseContentRoot(Path.GetDirectoryName(path))
                .ConfigureAppConfiguration(cb =>
                {
                    
                })
                .UseStartup<Startup>();


            var testServer = new TestServer(hostBuilder);

            //testServer.Host
            //    .MigrateDbContext<CatalogContext>((context, services) =>
            //    {
            //        var env = services.GetService<IWebHostEnvironment>();
            //        var settings = services.GetService<IOptions<CatalogSettings>>();
            //        var logger = services.GetService<ILogger<CatalogContextSeed>>();

            //        new CatalogContextSeed()
            //        .SeedAsync(context, env, settings, logger)
            //        .Wait();
            //    })
            //    .MigrateDbContext<IntegrationEventLogContext>((_, __) => { });

            return testServer;
        }
    }
}
