﻿using AutoMapper;
using Building.Infrastructure.DTOs;
using Building.Infrastructure.Exceptions;
using Building.Infrastructure.Models;
using Building.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Building.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BuildingController : ControllerBase
    {
        private readonly PropertyRepository _propertyRepository;
        private readonly OwnerRepository _ownerRepository;
        private readonly PropertyImageRepository _propertyImageRepository;
        private readonly PropertyTraceRepository _propertyTraceRepository;
        private readonly IMapper _mapper;

        public BuildingController(
            PropertyRepository propertyRepository,
            PropertyImageRepository propertyImageRepository,
            PropertyTraceRepository propertyTraceRepository,
            OwnerRepository ownerRepository,
            IMapper mapperBuilding
            )
        {
            _propertyRepository = propertyRepository;
            _ownerRepository = ownerRepository;
            _mapper = mapperBuilding;
            _propertyImageRepository = propertyImageRepository;
            _propertyTraceRepository = propertyTraceRepository;
        }

        //POST api/v1/[controller]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<ActionResult<PropertyDTO>> Post([FromBody] PropertyDTO propertyDto)
        {
            if (propertyDto.IdOwner == 0)
                return BadRequest();

            Property property = _mapper.Map<Property>(propertyDto);
            var owner = await _ownerRepository.Get(property.IdOwner);
            if (owner == null)
                throw new OwnerDomainException("Owner doesn't exists.");

            await _propertyRepository.Add(property);

            return CreatedAtAction(nameof(Get), new { id = property.Id }, null);
        }

        // GET: api/[controller]
        [HttpGet]
        [ProducesResponseType(typeof(PropertyDTO), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<List<PropertyDTO>>> Get()
        {
            return _mapper.Map<List<PropertyDTO>>(await _propertyRepository.GetAll());
        }

        // GET: api/[controller]/5
        [HttpGet("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(PropertyDTO), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<PropertyDTO>> Get(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var entity = await _propertyRepository.Get(id);
            if (entity == null)
            {
                return NotFound();
            }
            return _mapper.Map<PropertyDTO>(entity);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, PropertyDTO entity)
        {
            if (id != entity.Id)
            {
                return BadRequest();
            }

            await _propertyRepository.Update(_mapper.Map<Property>(entity));

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var entity = await _propertyRepository.Get(id);
            if (entity == null)
            {
                return NotFound();
            }

            var propertyImageList = await _propertyImageRepository.GetByPropertyId(id);
            if (propertyImageList !=null && propertyImageList.Count > 0)
                throw new PropertyImageDomainException("The property cannot be deleted because it has images assigned.");

            var propertyTraceList = await _propertyTraceRepository.GetByPropertyId(id);
            if (propertyTraceList != null && propertyTraceList.Count > 0)
                throw new PropertyTraceDomainException("The property cannot be deleted because it has traces assigned.");
            
            await _propertyRepository.Delete(id);
            return NoContent();
        }

        [HttpGet]
        [Route("items/search/")]
        [ProducesResponseType(typeof(PropertyDTO), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<List<PropertyDTO>>> ItemsWithSearchAsync([FromQuery] int? propetyTypeId, [FromQuery] decimal? priceMin, [FromQuery] decimal? priceMax, [FromQuery] int pageSize = 10, [FromQuery] int pageIndex = 0)
        {
            var root = (await _propertyRepository.GetAll()).AsQueryable();

            if (propetyTypeId.HasValue && propetyTypeId > 0)
            {
                root = root.Where(ci => ci.IdPropertyType == propetyTypeId);
            }

            if ( priceMin != null && priceMin > 0) 
            {
                var actualMin = priceMin.Value;
                root = root.Where(d => d.Price >= actualMin);
            }
            if (priceMax != null && priceMax > 0)
            {
                var actualMax = priceMax.Value;
                root = root.Where(d => d.Price <= actualMax);
            }

            var itemsOnPage = root
            .Skip(pageSize * pageIndex)
            .Take(pageSize);

            return _mapper.Map<List<PropertyDTO>>(itemsOnPage);
        }
    }
}
