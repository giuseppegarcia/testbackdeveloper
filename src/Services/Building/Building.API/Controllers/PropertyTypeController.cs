﻿using AutoMapper;
using Building.Infrastructure.DTOs;
using Building.Infrastructure.Models;
using Building.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Building.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PropertyTypeController : MyMDBController<PropertyType, PropertyTypeRepository, PropertyTypeDTO>
    {
        public PropertyTypeController(PropertyTypeRepository repository, IMapper mapper) : base(repository, mapper)
        {

        }
    }
}
