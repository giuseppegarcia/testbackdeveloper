﻿using AutoMapper;
using Building.Infrastructure.Models;
using Building.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Building.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class MyMDBController<TEntity, TRepository,TEntityDTO> : ControllerBase
        where TEntity : class, IEntity
        where TRepository : IRepository<TEntity>
        where TEntityDTO : class, IEntityDTO
    {
        private readonly TRepository repository;
        private readonly IMapper _mapper;

        public MyMDBController(TRepository repository,IMapper mapper)
        {
            this.repository = repository;
            _mapper = mapper;
        }


        // GET: api/[controller]
        [HttpGet]
        [ProducesResponseType(typeof(IEntityDTO), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<TEntityDTO>>> Get()
        {
            return _mapper.Map<List<TEntityDTO>>(await repository.GetAll());
        }

        // GET: api/[controller]/5
        [HttpGet("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(IEntityDTO), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEntityDTO>> Get(int id)
        {
            var entity = await repository.Get(id);
            if (entity  == null)
            {
                return NotFound();
            }
            return _mapper.Map<TEntityDTO>(entity);
        }

        // PUT: api/[controller]/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, TEntity entity)
        {
            if (id != entity.Id)
            {
                return BadRequest();
            }
            await repository.Update(entity);
            return NoContent();
        }

        // POST: api/[controller]
        [HttpPost]
        public async Task<ActionResult<TEntity>> Post(TEntity entity)
        {
            await repository.Add(entity);
            return CreatedAtAction("Get", new { id = entity.Id }, entity);
        }

        // DELETE: api/[controller]/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var entity = await repository.Get(id);
            if (entity == null)
            {
                return NotFound();
            }

            await repository.Delete(id);

            return NoContent();
        }

    }
}
