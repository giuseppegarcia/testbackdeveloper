﻿using AutoMapper;
using Building.Infrastructure.DTOs;
using Building.Infrastructure.Models;
using Building.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Building.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OwnerController : ControllerBase
    {
        private readonly OwnerRepository _repository;
        private readonly IMapper _mapper;

        public OwnerController(OwnerRepository ownerRepository,IMapper mapper)
        {
            _repository = ownerRepository;
            _mapper = mapper;
        }

        // GET: api/[controller]
        [HttpGet]
        [ProducesResponseType(typeof(OwnerDTO), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<List<OwnerDTO>>> Get()
        {
            return _mapper.Map<List<OwnerDTO>>(await _repository.GetAll());
        }

        // GET: api/[controller]/5
        [HttpGet("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(OwnerDTO), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<OwnerDTO>> Get(int id)
        {
            var entity = await _repository.Get(id);
            if (entity == null)
            {
                return NotFound();
            }
            return _mapper.Map<OwnerDTO>(entity);
        }

        // PUT: api/[controller]/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, OwnerDTO entity)
        {
            if (id != entity.Id)
            {
                return BadRequest();
            }

            await _repository.Update(_mapper.Map<Owner>(entity));

            return NoContent();
        }

        // POST: api/[controller]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<ActionResult<OwnerDTO>> Post(OwnerDTO entity)
        {
            var owner = _mapper.Map<Owner>(entity);
            await _repository.Add(owner);
            return CreatedAtAction("Get", new { id = owner.Id },owner);
        }

        // DELETE: api/[controller]/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var entity = await _repository.Get(id);
            if (entity == null)
            {
                return NotFound();
            }

            await _repository.Delete(id);
            return NoContent();
        }
    }
}
