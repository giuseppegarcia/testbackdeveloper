﻿using AutoMapper;
using Building.Infrastructure.DTOs;
using Building.Infrastructure.Exceptions;
using Building.Infrastructure.Models;
using Building.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Building.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PropertyImageController : ControllerBase
    {
        private readonly PropertyImageRepository _repository;
        private readonly PropertyRepository _propertyRepository;
        private readonly IMapper _mapper;

        public PropertyImageController(PropertyImageRepository propertyImageRepository, PropertyRepository propertyRepository, IMapper mapper)
        {
            _repository = propertyImageRepository;
            _propertyRepository = propertyRepository;
            _mapper = mapper;
        }

        // GET: api/[controller]
        [HttpGet]
        [ProducesResponseType(typeof(PropertyImageDTO), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<List<PropertyImageDTO>>> Get()
        {
            return _mapper.Map<List<PropertyImageDTO>>(await _repository.GetAll());
        }

        // GET: api/[controller]/5
        [HttpGet("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(PropertyImageDTO), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<PropertyImageDTO>> Get(int id)
        {
            var entity = await _repository.Get(id);
            if (entity == null)
            {
                return NotFound();
            }
            return _mapper.Map<PropertyImageDTO>(entity);
        }

        // PUT: api/[controller]/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, PropertyImageDTO entity)
        {
            if (id != entity.Id)
            {
                return BadRequest();
            }

            await _repository.Update(_mapper.Map<PropertyImage>(entity));

            return NoContent();
        }

        // POST: api/[controller]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<ActionResult<PropertyImageDTO>> Post(int idProperty, [FromForm]IFormFile formFile)
        {
            var property = await _propertyRepository.Get(idProperty);
            if (property == null)
                throw new PropertyDomainException("Property doesn't exists.");

            if (formFile == null || formFile.Length == 0)
                throw new PropertyImageDomainException("Files doesn't exists.");

            //Getting FileName
            var fileName = Path.GetFileName(formFile.FileName);
            //Getting file Extension
            var fileExtension = Path.GetExtension(fileName);
            // concatenating  FileName + FileExtension
            var newFileName = String.Concat(Convert.ToString(Guid.NewGuid()), fileExtension);

            var propertyImage = new PropertyImage()
            {
                IdProperty = idProperty,
                Name = newFileName,
                FileType = fileExtension,
                Enabled = true
            };

            using (var target = new MemoryStream())
            {
                await formFile.CopyToAsync(target);
                propertyImage.DataFiles = target.ToArray();
            }

            await _repository.Add(propertyImage);

            return Ok();
        }

        // DELETE: api/[controller]/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var entity = await _repository.Get(id);
            if (entity == null)
            {
                return NotFound();
            }

            await _repository.Delete(id);
            return NoContent();
        }
    }
}
