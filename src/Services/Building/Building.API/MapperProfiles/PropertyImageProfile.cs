﻿using AutoMapper;
using Building.Infrastructure.DTOs;
using Building.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Building.API.MapperProfiles
{
    public class PropertyImageProfile : Profile
    {
        public PropertyImageProfile()
        {
            CreateMap<PropertyImage, PropertyImageDTO>();
            CreateMap<PropertyImageDTO, PropertyImage>();
        }
    }
}
