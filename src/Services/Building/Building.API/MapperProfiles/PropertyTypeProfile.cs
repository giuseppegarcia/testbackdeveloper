﻿using AutoMapper;
using Building.Infrastructure.DTOs;
using Building.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Building.API.MapperProfiles
{
    public class PropertyTypeProfile : Profile
    {
        public PropertyTypeProfile()
        {
            CreateMap<PropertyType, PropertyTypeDTO>();
            CreateMap<PropertyTypeDTO, PropertyType>();
        }
    }
}
