﻿using Building.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;

namespace Building.Repository
{
    public class PropertyRepository : EfCoreRepository<Property, WeeloContext>
    {
        public PropertyRepository(WeeloContext context) : base(context)
        {
        }
    }
}
