﻿using Building.Infrastructure.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Building.Repository
{
    public class PropertyTraceRepository : EfCoreRepository<PropertyTrace, WeeloContext>
    {
        private readonly WeeloContext context;

        public PropertyTraceRepository(WeeloContext context) : base(context)
        {
            this.context = context;
        }
        public async Task<List<PropertyTrace>> GetByPropertyId(int propertyId)
        {
            return await context.Propertytraces.Where(pi => pi.IdProperty.Equals(propertyId)).ToListAsync();
        }
    }
}
