﻿using Building.Infrastructure.Models;
using System.Threading.Tasks;

namespace Building.Repository
{
    public interface IPropertyImageRepository
    {
        public Task<PropertyImage> Add(PropertyImage propertyImage);
        public Task<PropertyImage> GetById(int id);
        public void Update(PropertyImage propertyImage);
        public Task AddOrUpdate(PropertyImage propertyImage);
    }
}