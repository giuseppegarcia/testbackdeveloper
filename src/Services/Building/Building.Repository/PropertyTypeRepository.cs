﻿using Building.Infrastructure.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Building.Repository
{
    public class PropertyTypeRepository : EfCoreRepository<PropertyType, WeeloContext>
    {
        public PropertyTypeRepository(WeeloContext context) : base(context)
        {
        }
    }
}
