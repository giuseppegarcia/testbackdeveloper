﻿using Building.Infrastructure.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Building.Repository
{
    public class OwnerRepository : EfCoreRepository<Owner, WeeloContext>
    {
        public OwnerRepository(WeeloContext weeloContext) : base(weeloContext)
        {
        }
    }
}
