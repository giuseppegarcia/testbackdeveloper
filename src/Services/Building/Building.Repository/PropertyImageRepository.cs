﻿using Building.Infrastructure.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Building.Repository
{
    public class PropertyImageRepository : EfCoreRepository<PropertyImage, WeeloContext>
    {
        private readonly WeeloContext context;
        public PropertyImageRepository(WeeloContext weeloContext) : base(weeloContext)
        {
            context = weeloContext;
        }

        public async Task<List<PropertyImage>> GetByPropertyId(int propertyId)
        {
            return await context.PropertyImages.Where(pi => pi.IdProperty.Equals(propertyId)).ToListAsync();
        }
    }
}
