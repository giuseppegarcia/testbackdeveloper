﻿using Building.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Building.Repository
{
    public interface IPropertyRepository : IRepository<Property>
    {
    }
}
