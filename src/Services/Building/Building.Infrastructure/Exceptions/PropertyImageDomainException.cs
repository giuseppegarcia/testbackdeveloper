﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Building.Infrastructure.Exceptions
{
    public class PropertyImageDomainException : Exception
    {
        public PropertyImageDomainException()
        { }

        public PropertyImageDomainException(string message)
            : base(message)
        { }

        public PropertyImageDomainException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}