﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Building.Infrastructure.Exceptions
{
    public class OwnerDomainException : Exception
    {
        public OwnerDomainException()
        { }

        public OwnerDomainException(string message)
            : base(message)
        { }

        public OwnerDomainException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}