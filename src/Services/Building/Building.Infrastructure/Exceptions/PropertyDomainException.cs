﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Building.Infrastructure.Exceptions
{
    public class PropertyDomainException : Exception
    {
        public PropertyDomainException()
        { }

        public PropertyDomainException(string message)
            : base(message)
        { }

        public PropertyDomainException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}