﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Building.Infrastructure.Exceptions
{
    public class PropertyTraceDomainException : Exception
    {
        public PropertyTraceDomainException()
        { }

        public PropertyTraceDomainException(string message)
            : base(message)
        { }

        public PropertyTraceDomainException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}