﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Building.Infrastructure.DTOs
{
    public class PropertyImageDTO 
    {
        public int Id { get; set; }
        public int IdProperty { get; set; }
        public bool Enabled { get; set; }
        public string Name { get; set; }
        public string FileType { get; set; }
        public byte[] DataFiles { get; set; }
    }
}
