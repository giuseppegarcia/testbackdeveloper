﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Building.Infrastructure.DTOs
{
    public class PropertyDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public decimal Price { get; set; }
        public string CodeInternal { get; set; }
        public int NumberBedroom { get; set; }
        public int IdPropertyType { get; set; }
        public int NumberGarageSpace { get; set; }
        public int IdConstructionType { get; set; }
        public string Details { get; set; }
        public decimal TotalArea { get; set; }
        public int IdOwner { get; set; }
    }
}
