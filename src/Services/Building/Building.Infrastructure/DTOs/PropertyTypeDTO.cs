﻿using Building.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Building.Infrastructure.DTOs
{
    public class PropertyTypeDTO : IEntityDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
