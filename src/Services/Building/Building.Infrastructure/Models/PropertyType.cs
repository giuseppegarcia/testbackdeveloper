﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Building.Infrastructure.Models
{
    public partial class PropertyType : IEntity
    {
        public PropertyType()
        {
            Properties = new HashSet<Property>();
        }
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Property> Properties { get; set; }
    }
}
