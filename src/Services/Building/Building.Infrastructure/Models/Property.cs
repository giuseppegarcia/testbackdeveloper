﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Building.Infrastructure.Models
{
    public partial class Property : IEntity
    {
        public Property()
        {
            PropertyImages = new HashSet<PropertyImage>();
            Propertytraces = new HashSet<PropertyTrace>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public decimal Price { get; set; }
        public string CodeInternal { get; set; }
        public int NumberBedroom { get; set; }
        public int IdPropertyType { get; set; }
        public int NumberGarageSpace { get; set; }
        public string Details { get; set; }
        public decimal TotalArea { get; set; }
        public int IdOwner { get; set; }

        public virtual Owner IdOwnerNavigation { get; set; }
        public virtual PropertyType IdPropertyTypeNavigation { get; set; }
        public virtual ICollection<PropertyImage> PropertyImages { get; set; }
        public virtual ICollection<PropertyTrace> Propertytraces { get; set; }
    }
}
