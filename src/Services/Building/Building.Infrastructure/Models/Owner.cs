﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Building.Infrastructure.Models
{
    public partial class Owner : IEntity
    {
        public Owner()
        {
            Properties = new HashSet<Property>();
        }

        public string Name { get; set; }
        public string Address { get; set; }
        public string Photo { get; set; }
        public DateTime Birthday { get; set; }

        public virtual ICollection<Property> Properties { get; set; }
        public int Id { get; set; }
    }
}
