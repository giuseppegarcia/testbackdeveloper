﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Building.Infrastructure.Models
{
    public partial class PropertyImage : IEntity 
    {
        public int Id { get; set; }
        public int IdProperty { get; set; }
        public bool Enabled { get; set; }
        public string Name { get; set; }
        public string FileType { get; set; }
        public byte[] DataFiles { get; set; }

        public virtual Property IdPropertyNavigation { get; set; }
    }
}
