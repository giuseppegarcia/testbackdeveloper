﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Building.Infrastructure.Models
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
